This dotCMS static configuration plugin performs the configuration of the Tomcat server on which dotCMS runs. This includes server connectors, SSL certificates, etc.

## Installation
### From Release
* Download the most recent release archive from [this plugin's repository's tags page](https://gitlab.msu.edu/canr/edu.msu.anr.config.server/tags).
* Extract the release archive to the 'plugins' directory of your dotCMS instance. This should create the directory 'plugins\edu.msu.anr.config.server'.

## Configuration
In order to configure the plugin for your specific instance, you will need to create and edit a 'plugin.properties' file in the plugin's 'conf' directory.

* Copy the file 'plugin.properties.dist' in the plugin's top level directory to 'plugin.properties'.
```
> cd plugins\edu.msu.anr.config.server
> cp conf\plugin.properties.dist conf\plugin.properties
```
* Edit the file 'plugin.properties' to replace the dummy credentials with the credentials for your database.

## Deploying
In order to deploy the configuration plugin to your dotCMS instance, you must run the dotCMS deploy plugins script. Note that this will deploy all of your static plugins.

```
> cd E:\dotcms\dotcms_x.y.z\
> .\bin\deploy-plugins.bat
```
